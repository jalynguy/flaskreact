from flask import Flask
from flask_restful import Api, Resource, reqparse
import psycopg2
import uuid
import psycopg2.extras

app = Flask(__name__)

def get_db_connection():
    conn = psycopg2.connect(host='localhost',
                            database='VelosysPractice',
                            user='postgres',
                            password='welcomeJN98')
    return conn


@app.route("/members")
def members():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('SELECT * FROM "Users";')
    users = cur.fetchall()
    cur.close()
    conn.close()
    return {"members":["Member1", "Member2"]}

if __name__ == "__main__":
    app.run(debug=True)