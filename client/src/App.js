import "./App.css"
import React, {useState, useEffect} from 'react'
import Sidebar from "./components/Sidebar"


function App(){
  const [data, setData] = useState([{}])
  useEffect(() =>{
    fetch("/members").then(
      res => res.json()
    ).then(
      data => {
        setData(data)
        console.log(data)
      }
    )
  }, [])
  return (
    <div className="App"> 
      <Sidebar />
      {(typeof data.members === 'undefined') ? (
        <p>Loading...</p>
      ): (
        data.members.map((member,i) => (
          <p key={i}></p>
        ))
      )
      }

    </div>
  )
}

export default App
