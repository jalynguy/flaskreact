import HomeIcon from '@mui/icons-material/Home';
import AddIcon from '@mui/icons-material/Add';

export const SidebarData = [  
    {    
        title: "Home",
        icon: <HomeIcon />,
        link: "/home"
    },
    {
        title: "Add Event",
        icon: <AddIcon />,
        link: "/AddEvent"
    }
]